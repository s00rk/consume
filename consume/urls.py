"""consume URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.decorators import login_required

from app import views as app_view
from establishment import views as e_views

urlpatterns = [

    #Admin
    url(r'^login/', app_view.LoginView.as_view()),
    url(r'^recover/', app_view.ResetView.as_view()),
    url(r'^reset/', app_view.ResetView.as_view()),
    url(r'^logout/', app_view.LogoutView.as_view()),
    url(r'^$', login_required(app_view.HomeView.as_view())),
    url(r'^profile/$', login_required(app_view.UserView.as_view())),
    url(r'^users/$', login_required(e_views.UserView.as_view())),
    url(r'^users/(?P<pk>\w+)/$', login_required(e_views.UserDetailView.as_view())),
    url(r'^schools/$', login_required(e_views.SchoolView.as_view())),
    url(r'^schools/(?P<pk>\w+)/$', login_required(e_views.SchoolDetailView.as_view())),
    url(r'^students/$', login_required(e_views.StudentView.as_view())),
    url(r'^students/(?P<pk>\w+)/$', login_required(e_views.StudentDetailView.as_view())),

    url(r'^report/(?P<pk>\w+)/$', app_view.ReportView.as_view()),
    url(r'^events/$', login_required(app_view.EventView.as_view())),
    url(r'^events/(?P<pk>\w+)/$', login_required(app_view.EventDetailView.as_view())),

    url(r'^download_qr/', app_view.DownloadQRView.as_view()),

    #Api
    url(r'^api/login_school/$', e_views.LoginSchoolApi.as_view()),
    url(r'^api/users/$', login_required(e_views.UserApi.as_view())),
    url(r'^api/schools/$', login_required(e_views.SchoolApi.as_view())),
    url(r'^api/students/$', login_required(e_views.StudentApi.as_view())),
    url(r'^api/states/$', login_required(e_views.StateApi.as_view())),
    url(r'^api/cities/(?P<pk>\w+)/$', login_required(e_views.CityApi.as_view())),
    url(r'^api/events/$', login_required(app_view.EventApi.as_view())),
    url(r'^api/validate/$', app_view.ValidateQR.as_view()),

    url(r'^admin/', admin.site.urls),
]
