# -*- coding: utf-8 -*-
from django.conf import settings
from django.db.models import Q
from django.http import HttpResponse
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic.base import TemplateView

from django.contrib import messages
from django.core.signing import Signer
from django.contrib.auth import authenticate
import json, base64
from django.utils import timezone

from django.contrib.auth.models import User
from .models import State, City, School, Student
from app.models import EventSchool, EventSchoolStudent
from app.tasks import sendToUser, sendToSchool

class UserView(TemplateView):
    template_name = 'users.html'
class UserDetailView(TemplateView):
    template_name = 'user_detail.html'
    def get(self, request, pk, *args, **kwargs):
        if pk is None or pk == 'add':
            return render(request, self.template_name, { 'title': 'Agregar Usuario' })
        user = get_object_or_404(User, pk=pk)
        return render(request, self.template_name, { 'title': 'Editar ' + user.first_name + ' ' + user.last_name, 'model': user })
    def post(self, request, pk, *args, **kwargs):
        if pk is None or pk == 'add':
            user = User()
            title = 'Agregar Usuario'
        else:
            user = get_object_or_404(User, pk=pk)
            title = 'Editar ' + user.first_name + ' ' + user.last_name

        email = request.POST.get('email', None)
        first_name = request.POST.get('first_name', None)
        last_name = request.POST.get('last_name', None)
        is_superuser = request.POST.get('is_superuser', 'off')

        if email is None or first_name is None or last_name is None or is_superuser is None:
            messages.error(request, 'Faltan parametros')
            return render(request, self.template_name, { 'title': title, 'model': user })

        if User.objects.filter(email=email).exclude(pk=user.pk).exists():
            messages.error(request, 'Ya existe un usuario con ese email')
            return render(request, self.template_name, { 'title': title, 'model': user })

        if is_superuser == 'on':
            is_superuser = True
            is_staff = True
        else:
            is_superuser = False
            is_staff = True

        send_mail = False
        textMessage = 'Usuario actualizado satisfactoriamente'
        if user.pk is None:
            password = User.objects.make_random_password()
            user.set_password(password)
            textMessage = 'Usuario creado satisfactoriamente'
            send_mail = True
        user.email = email
        user.username = email
        user.first_name = first_name
        user.last_name = last_name
        user.is_staff = is_staff
        user.is_superuser = is_superuser
        user.save()

        if send_mail:
            template_info = {
                'link': 'http://' + request.get_host() + '/',
                'email': email,
                'password': password,
                'first_name': first_name
            }
            sendToUser.delay(template_info)

        messages.success(request, textMessage)
        return redirect('/users')

class SchoolView(TemplateView):
    template_name = 'schools.html'
class SchoolDetailView(TemplateView):
    template_name = 'school_detail.html'
    def get(self, request, pk, *args, **kwargs):
        cities = City.objects.filter(state__pk=25).order_by('name')
        if pk is None or pk == 'add':
            return render(request, self.template_name, { 'title': 'Agregar Escuela', 'cities': cities })
        school = get_object_or_404(School, pk=pk)
        return render(request, self.template_name, { 'title': 'Editar ' + school.name, 'model': school, 'cities': cities })
    def post(self, request, pk, *args, **kwargs):
        states = State.objects.order_by('name')

        if pk is None or pk == 'add':
            school = School()
            title = 'Agregar Escuela'
        else:
            school = get_object_or_404(School, pk=pk)
            title = 'Editar ' + school.name

        email = request.POST.get('email', None)
        address = request.POST.get('address', None)
        name = request.POST.get('name', None)
        city = request.POST.get('city', None)

        if (email is None and school.pk is None) or address is None or name is None or city is None:
            messages.error(request, 'Faltan parametros')
            return render(request, self.template_name, { 'title': title, 'model': school, 'states': states })

        if School.objects.filter(name=name).exclude(pk=school.pk).exists():
            messages.error(request, 'Ya existe una escuela con ese nombre')
            return render(request, self.template_name, { 'title': title, 'model': school, 'states': states })
        elif User.objects.filter(username=email).exclude(pk=school.pk).exists():
            messages.error(request, 'Ya existe una escuela utilizando ese correo')
            return render(request, self.template_name, { 'title': 'Agregar Escuela', 'model': school, 'states': states })

        textMessage = 'Escuela actualizada satisfactoriamente'
        if school.pk is None:
            textMessage = 'Escuela creada satisfactoriamente'
            password = User.objects.make_random_password()

            template_info = {
                'link': 'http://' + request.get_host() + settings.STATIC_URL + 'apk/comedor.apk',
                'email': email,
                'password': password,
                'first_name': name
            }
            sendToSchool.delay(template_info)

        city = get_object_or_404(City, pk=city)

        if school.pk is None:
            user = User()
            user.username = email
            user.email = email
            user.set_password(password)
            user.save()
            school.user = user

        school.name = name
        school.address = address
        school.city = city
        school.save()

        messages.success(request, textMessage)
        return redirect('/schools')

class StudentView(TemplateView):
    template_name = 'students.html'
class StudentDetailView(TemplateView):
    template_name = 'student_detail.html'
    def get(self, request, pk, *args, **kwargs):
        cities = City.objects.filter(state__pk=25).order_by('name')
        schools = School.objects.order_by('name')
        if pk is None or pk == 'add':
            return render(request, self.template_name, { 'title': 'Agregar Alumno', 'schools': schools, 'cities': cities })
        student = get_object_or_404(Student, pk=pk)
        return render(request, self.template_name, { 'title': 'Editar ' + student.first_name + ' ' + student.last_name, 'model': student, 'schools': schools, 'cities': cities })
    def post(self, request, pk, *args, **kwargs):
        schools = School.objects.order_by('name')
        cities = City.objects.filter(state__pk=25).order_by('name')
        if pk is None or pk == 'add':
            student = Student()
            title = 'Agregar Alumno'
        else:
            student = get_object_or_404(Student, pk=pk)
            title = 'Editar ' + student.first_name + ' ' + student.last_name

        curp = request.POST.get('curp', None)
        first_name = request.POST.get('first_name', None)
        last_name = request.POST.get('last_name', None)
        school = request.POST.get('school', None)
        career = request.POST.get('career', None)
        city = request.POST.get('city', None)
        email = request.POST.get('email', None)

        if email is None or city is None or school is None or last_name is None or first_name is None or curp is None:
            messages.error(request, 'Faltan parametros')
            return render(request, self.template_name, { 'title': title, 'model': school })

        school = get_object_or_404(School, pk=school)

        if Student.objects.filter(email=email, school=school).exclude(pk=student.pk).exists():
            messages.error(request, 'Ya existe un estudiante con ese correo')
            return render(request, self.template_name, { 'title': title, 'model': student, 'schools': schools, 'cities': cities })
        elif Student.objects.filter(curp=curp, school=school).exclude(pk=student.pk).exists():
            messages.error(request, 'Ya existe un estudiante con ese curp')
            return render(request, self.template_name, { 'title': title, 'model': school, 'schools': schools, 'cities': cities })

        city = get_object_or_404(City, pk=city)

        textMessage = 'Alumno actualizado satisfactoriamente'
        if student.pk is None:
            textMessage = 'Alumno creado satisfactoriamente'

        student.first_name = first_name
        student.last_name = last_name
        student.city = city
        student.curp = curp
        student.school = school
        student.career = career
        student.email = email
        student.save()

        messages.success(request, textMessage)
        return redirect('/students')

class JsonResponse(HttpResponse):
    def __init__(self, content, mimetype='application/json', status=None, content_type='application/json; charset=utf-8'):
        super(JsonResponse, self).__init__(
            content=json.dumps(content, ensure_ascii=False),
            status=status,
            content_type=content_type
        )

class StateApi(View):
    def get(self, request, *args, **kwargs):
        states = list(State.objects.values('id', 'name').all())
        return JsonResponse(states)
class CityApi(View):
    def get(self, request, pk = 25, *args, **kwargs):
        cities = list(City.objects.filter(state__pk=pk).values('id', 'name'))
        return JsonResponse(cities)

@method_decorator(csrf_exempt, name='dispatch')
class LoginSchoolApi(View):
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body.decode("utf-8"))
        error = JsonResponse({ 'error': 'Usuario/Contraseña incorrecta' })

        if 'email' not in data or 'password' not in data:
            return error

        email = data['email']
        password = data['password']

        user = authenticate(username=email, password=password)
        if user is None:
            return error

        try:
            if user.school is None:
                pass
        except:
            return JsonResponse({ 'error': 'Este usuario no tiene una escuela asignada' })

        if EventSchool.objects.filter(school=user.school.id, event__start_date__lte=timezone.now(), event__end_date__gte=timezone.now(), active=True).exists():
            signer = Signer()
            identifier = signer.sign(str(user.pk) + '-' + user.email)
            return JsonResponse({ 'success': identifier })
        else:
            return JsonResponse({ 'error': 'La escuela no se encuentra dentro de ningun periodo activo' })

class UserApi(View):
    def get(self, request, *args, **kwargs):
        total = User.objects.filter(Q(is_staff=True) | Q(is_superuser=True)).count()

        search = request.GET.get('search[value]', '').strip()

        start = int(request.GET.get('start', 0))
        end = int(request.GET.get('length', 10))

        if len(search) > 0:
            users = User.objects.filter(Q(is_staff=True) | Q(is_superuser=True),  Q(first_name__contains=search) | Q(last_name__contains=search) | Q(email__contains=search)).all()[start:end]
        else:
            users = User.objects.filter(Q(is_staff=True) | Q(is_superuser=True)).all()[start:end]
        result = [[obj.id, obj.email, obj.first_name + ' ' + obj.last_name, 'Super Usuario' if obj.is_superuser else 'Admin' ] for obj in users]
        output = {
            'draw': request.GET.get('draw', 1),
            'recordsTotal': total,
            'recordsFiltered': users.count(),
            'data': result

        }
        return JsonResponse(output)
    def post(self, request, *args, **kwargs):
        User.objects.filter(pk=request.POST.get('id', None)).delete()
        return JsonResponse({ 'success': True })
class SchoolApi(View):
    def get(self, request, *args, **kwargs):
        total = School.objects.count()

        search = request.GET.get('search[value]', '').strip()

        start = int(request.GET.get('start', 0))
        end = int(request.GET.get('length', 10))

        if len(search) > 0:
            schools = School.objects.filter(Q(name__contains=search) | Q(city__name__startswith=search)).all()[start:end]
        else:
            schools = School.objects.all()[start:end]
        result = [[obj.id, obj.name, obj.city.name, obj.total()] for obj in schools]
        output = {
            'draw': request.GET.get('draw', 1),
            'recordsTotal': total,
            'recordsFiltered': schools.count(),
            'data': result

        }
        return JsonResponse(output)
    def post(self, request, *args, **kwargs):
        school = School.objects.filter(pk=request.POST.get('id', None)).first()
        if school is not None:
            User.objects.filter(pk=school.user_id).delete()
            school.delete()
        return JsonResponse({ 'success': True })
class StudentApi(View):
    def getButton(self, url, student):
        eventstudent = EventSchoolStudent.objects.filter(student=student, event_school__event__start_date__lte=timezone.now(), event_school__event__end_date__gte=timezone.now(), active=True).order_by('-event_school__event__start_date').first()
        if eventstudent is None:
            return '<a href="#" class="mb-sm btn btn-warning">Sin periodo activo</a>'
        else:
            filename = 'QR_Comedor_' + student.last_name + '_' + student.first_name + '.pdf'
            return '<a href="#" onclick="return downloadQR(\'' + url + '\', \'' + filename + '\');" class="mb-sm btn btn-primary">Descargar PDF</a>'
    def get(self, request, *args, **kwargs):
        total = Student.objects.count()

        search = request.GET.get('search[value]', '').strip()

        start = int(request.GET.get('start', 0))
        end = int(request.GET.get('length', 10))

        idSchool = request.GET.get('id_school', None)

        if idSchool is None:
            if len(search) > 0:
                students = Student.objects.filter(Q(first_name__contains=search) | Q(last_name__contains=search) | Q(curp=search)).all()[start:end]
            else:
                students = Student.objects.all()[start:end]
        else:
            students = Student.objects.filter(school__pk=int(idSchool)).order_by('first_name', 'last_name')

        signer = Signer()
        urlPath = 'http://' + request.get_host() + '/download_qr/?code='

        result = [[obj.id, obj.school.name, obj.first_name, obj.last_name, obj.career, obj.email, self.getButton(urlPath + base64.b64encode(signer.sign(str(obj.pk)).encode('utf-8')).decode('utf-8'), obj)] for obj in students]
        output = {
            'draw': request.GET.get('draw', 1),
            'recordsTotal': total,
            'recordsFiltered': students.count(),
            'data': result
        }
        return JsonResponse(output)
    def post(self, request, *args, **kwargs):
        Student.objects.filter(pk=request.POST.get('id', None)).delete()
        return JsonResponse({ 'success': True })
