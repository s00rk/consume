from django.contrib import admin

from .models import School, Student, State, City
# Register your models here.
@admin.register(State)
class StateAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ['name']

@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'state')
    raw_id_fields = ('state',)
    search_fields = ['name']

@admin.register(School)
class SchoolAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'user', 'city')
    raw_id_fields = ('city', 'user')
    search_fields = ['name', 'user__email']

@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ('id', 'curp', 'first_name', 'last_name', 'city', 'school')
    raw_id_fields = ('city', 'school')
    search_fields = ['first_name', 'last_name', 'email', 'curp']
