from django.db import models

from django.contrib.auth.models import User

class State(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100, unique=True, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name.encode('utf-8')

class City(models.Model):
    id = models.IntegerField(primary_key=True)
    state = models.ForeignKey(State, db_index=True)
    name = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = 'Cities'

    def __str__(self):
        return self.name.encode('utf-8')

class School(models.Model):
    user = models.OneToOneField(User)
    name = models.CharField(max_length=100, unique=True, db_index=True)
    city = models.ForeignKey(City)
    address = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def getName(self):
        return u''.join(self.name).encode('utf-8')

    def total(self):
        return Student.objects.filter(school__pk=self.pk).count()

    def __str__(self):
        return self.name.encode('utf-8')

class Student(models.Model):
    curp = models.CharField(max_length=100, db_index=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    school = models.ForeignKey(School)
    career = models.CharField(max_length=200, blank=True)
    city = models.ForeignKey(City)
    email = models.EmailField(db_index=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s %s - %s' % (self.first_name.encode('utf-8'), self.last_name.encode('utf-8'), self.school.name.encode('utf-8'))
