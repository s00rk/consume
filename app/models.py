from django.db import models

from django.contrib.auth.models import User
from establishment.models import School, Student

from django.db.models.functions import TruncMonth
from django.db.models import Count
from django.db.models.signals import post_save
from django.dispatch import receiver
import datetime


class UserInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    token = models.CharField(max_length=100, blank=True, db_index=True)
    token_at = models.DateTimeField(null=True, blank=True)


class Event(models.Model):
    name = models.CharField(max_length=100, db_index=True)
    start_date = models.DateField()
    end_date = models.DateField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def total_students(self):
        resp = 0
        for school in self.eventschool_set.all():
            resp += school.eventschoolstudent_set.filter(active=True).count()
        return resp

    def schools(self):
        return self.eventschool_set.filter(active=True).order_by('school__name')

    def translateMonth(self, name):
        if name == 'Jan':
            return 'Ene'
        elif name == 'Apr':
            return 'Abr'
        elif name == 'Aug':
            return 'Ago'
        elif name == 'Dec':
            return 'Dic'
        return name

    def getMonths(self):
        diff = (self.end_date.year - self.start_date.year) * 12 + self.end_date.month - self.start_date.month
        months = []
        for monthInt in range(diff+1):
            monthinteger = (monthInt + self.start_date.month)
            if monthinteger > 12:
                monthinteger = monthinteger - 12
            months.append( self.translateMonth(datetime.date(1900, monthinteger, 1).strftime('%b')) )
        return months

    def __str__(self):
        return self.name.encode('utf-8')

class EventSchool(models.Model):
    event = models.ForeignKey(Event)
    school = models.ForeignKey(School)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def students(self):
        return self.eventschoolstudent_set.filter(active=True).order_by('student__first_name')

    def __str__(self):
        return '%s %s' % (self.event, self.school)

class EventSchoolStudent(models.Model):
    event_school = models.ForeignKey(EventSchool)
    student = models.ForeignKey(Student)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def consumeByMonth(self):
        start_date = self.event_school.event.start_date
        end_date = self.event_school.event.end_date
        diff = (end_date.year - start_date.year) * 12 + end_date.month - start_date.month
        months = []

        for monthInt in range(diff+1):
            monthinteger = (monthInt + start_date.month)
            if monthinteger > 12:
                monthinteger = monthinteger - 12
            count = self.studentevent_set.filter(date__month=monthinteger).count()
            months.append( count )
        return months

    def __str__(self):
        return '%s %s' % (self.event_school, self.student)

class StudentEvent(models.Model):
    event_student = models.ForeignKey(EventSchoolStudent)
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return '%s %s' % (self.event_student, self.date)

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserInfo.objects.create(user=instance)
