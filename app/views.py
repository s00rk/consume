# coding=utf-8
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic.base import TemplateView
from django.views import View
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.conf import settings

from django.core.signing import Signer
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.db.models import Q
from datetime import datetime

from establishment.views import JsonResponse
from establishment.models import School, Student
from .models import UserInfo, Event, EventSchool, EventSchoolStudent, StudentEvent
from .tasks import sendToStudent, sendToUserReset

import os, json, base64
from urllib import urlopen
from django.utils import timezone
from django.utils.encoding import smart_str
from subprocess import call

class ResetView(TemplateView):
    template_name = 'reset_password.html'
    def get(self, request):
        if request.user.is_authenticated:
            hasNext = request.GET.get('next', '/')
            return redirect(hasNext)
        token = request.GET.get('code', None)
        if token is not None:
            try:
                signer = Signer()
                code = signer.unsign(token).split('--')
                user = code[0]
                password = code[1]

                userinfo = UserInfo.objects.filter(user__pk=int(user), token=token).first()
                if userinfo is not None:
                    userinfo.user.set_password(password)
                    userinfo.user.save()
                    userinfo.token = ''
                    userinfo.save()
                    messages.success(request, 'Contraseña cambiada exitosamente!')
                    return redirect('/')
            except:
                pass
            messages.error(request, 'Codigo invalido')
        return render(request, self.template_name)
    def post(self, request):
        user = User.objects.filter(email=request.POST.get('email', None)).first()
        if user is not None and (user.is_staff or user.is_superuser):
            signer = Signer()
            if len(user.userinfo.token) == 0:
                password = User.objects.make_random_password()
                code = signer.sign(str(user.pk) + '--' + password)
                user.userinfo.token = code
                user.userinfo.token_at = datetime.now()
                user.userinfo.save()
            else:
                code = user.userinfo.token
                password = signer.unsign(code).split('--')[1]
            
            template_info = {
                'link': 'http://' + request.get_host() + '/reset/?code=' + code,
                'first_name': user.first_name,
                'email': user.email,
                'password': password
            }
            sendToUserReset.delay(template_info)
        messages.success(request, 'Si el correo existe, se le enviara un correo para restaurar la contraseña')
        hasNext = request.GET.get('next', '/')
        return redirect(hasNext)

class LoginView(TemplateView):
    template_name = 'login.html'
    def get(self, request):
        if request.user.is_authenticated:
            hasNext = request.GET.get('next', '/')
            return redirect(hasNext)
        return render(request, self.template_name)
    def post(self, request):
        user = authenticate(username=request.POST.get('email', None), password=request.POST.get('password', None))
        if user is None:
            messages.error(request, 'Usuario/Contraseña incorrecta')
            return render(request, self.template_name)
        elif (user.is_staff == False and user.is_superuser == False) or user.is_active == False:
            messages.error(request, 'No tienes permisos')
            return render(request, self.template_name)
        login(request, user)
        hasNext = request.GET.get('next', '/')
        return redirect(hasNext)

class LogoutView(View):
    def get(self, request, *args, **kwargs):
        logout(request)
        return redirect('/')

class HomeView(TemplateView):
    template_name = 'home.html'

    def get(self, request, *args, **kwargs):
        events = Event.objects.order_by('-start_date', 'name')
        return render(request, self.template_name, { 'events': events })

class UserView(TemplateView):
    template_name = 'user_info.html'
    def post(self, request, *args, **kwargs):
        email = request.POST.get('email', '')
        first_name = request.POST.get('first_name', None)
        last_name = request.POST.get('last_name', None)
        password = request.POST.get('password', '')
        new_password = request.POST.get('new_password', '')

        user = request.user
        if first_name is None or last_name is None or (len(email) > 0 and email != user.email and len(password) == 0) or (len(new_password) > 0 and len(password) == 0) :
            messages.error(request, 'Faltan parametros')
            return render(request, self.template_name)


        if User.objects.filter(email=email).exclude(pk=user.pk).exists():
            messages.error(request, 'Ya existe un usuario con ese email')
            return render(request, self.template_name)
        elif (len(new_password) > 0 or (len(email) > 0 and email != user.email)) and not user.check_password(password):
            messages.error(request, 'Contraseña incorrecta')
            return render(request, self.template_name)

        if len(new_password) > 0:
            user.set_password(new_password)
        user.email = email
        user.username = email
        user.first_name = first_name
        user.last_name = last_name
        user.save()
        messages.success(request, 'Perfil actualizado correctamente')
        return redirect('/')

class EventView(TemplateView):
    template_name = 'events.html'
class EventApi(View):
    def get(self, request, *args, **kwargs):
        total = Event.objects.count()

        search = request.GET.get('search[value]', '').strip()

        start = int(request.GET.get('start', 0))
        end = int(request.GET.get('length', 10))

        if len(search) > 0:
            events = Event.objects.filter(name__contains=search).all()[start:end]
        else:
            events = Event.objects.all()[start:end]
        result = [[obj.id, obj.name, (obj.start_date.strftime("%d/%m/%y") + ' - ' + obj.end_date.strftime("%d/%m/%y")), obj.eventschool_set.filter(active=True).count(), obj.total_students()] for obj in events]
        output = {
            'draw': request.GET.get('draw', 1),
            'recordsTotal': total,
            'recordsFiltered': events.count(),
            'data': result
        }
        return JsonResponse(output)
    def post(self, request, *args, **kwargs):
        Event.objects.filter(pk=request.POST.get('id', None)).delete()
        return JsonResponse({ 'success': True })

class EventDetailView(TemplateView):
    template_detail = 'event_detail.html'
    template_new = 'event_new.html'
    def get(self, request, pk, *args, **kwargs):
        if pk is None or pk == 'add':
            return render(request, self.template_new)
        event = get_object_or_404(Event, pk=pk)
        selecteds = []
        selectedsName = []
        for school in event.eventschool_set.filter(active=True).all():
            selecteds.append(int(school.school_id))
            students = []
            for student in school.eventschoolstudent_set.filter(active=True).values('student__id'):
                students.append(student['student__id'])
            selectedsName.append( { 'name': str(school.school_id) + '-' + school.school.name, 'students': students })

        return render(request, self.template_detail, { 'title': 'Editar ' + event.name, 'model': event, 'schools': School.objects.order_by('name'), 'selecteds': selecteds, 'selectedsName': selectedsName })
    def post(self, request, pk, *args, **kwargs):
        self.template_name = self.template_detail
        if pk is None or pk == 'add':
            self.template_name = self.template_new
        name = request.POST.get('name', None)
        start_date = request.POST.get('start_date', None)
        end_date = request.POST.get('end_date', None)

        if name is None or start_date is None or end_date is None:
            messages.error(request, 'Faltan parametros')
            return render(request, self.template_name)

        try:
            start_date = datetime.strptime(start_date, '%d/%m/%Y')
            end_date = datetime.strptime(end_date, '%d/%m/%Y')
        except:
            messages.error(request, 'Error en el formato de las fechas')
            return render(request, self.template_name)

        if pk is None or pk == 'add':
            if Event.objects.filter(name=name).exists():
                messages.error(request, 'Ya existe un periodo con este nombre')
                return render(request, self.template_name)
            event = Event()
            event.name = name
            event.start_date = start_date
            event.end_date = end_date
            event.save()
            messages.success(request, 'Periodo guardado satisfactoriamente')
            return redirect('/events/' + str(event.pk) + '/')

        event = get_object_or_404(Event, pk=pk)
        event.name = name
        event.start_date = start_date
        event.end_date = end_date
        event.save()
        students = json.loads(request.POST.get('students', '{}'))
        schools = []
        for key in students:
            id_students = students[key]
            key = key.split('-')[0]
            if len(id_students) == 0:
                continue
            schools.append( key )

            school = School.objects.filter(pk=key).first()
            if school is None:
                continue

            eventschool = EventSchool.objects.filter(school=school, event=event).order_by('-created_at').first()
            activate = False
            if eventschool is None:
                eventschool = EventSchool()
                eventschool.school = school
                eventschool.event = event
                eventschool.save()
                activate = True
            elif eventschool.active == False and (eventschool.event_id == event.id or event.start_date > eventschool.event.end_date):
                eventschool.active = True
                eventschool.save()
                activate = True
            elif eventschool.active == False and (eventschool.event_id != event.id or event.start_date < eventschool.event.end_date):
                messages.error(request, 'Esta escuela ya se encuentra en otro periodo registrado [' + school.name + ']')
            elif eventschool is not None and eventschool.active:
                activate = True

            if activate:
                for id_student in id_students:
                    student = Student.objects.filter(pk=id_student).first()
                    if student is None:
                        continue
                    eventschoolstudent = EventSchoolStudent.objects.filter(student=student, event_school=eventschool).first()

                    send_mail = False
                    if eventschoolstudent is None:
                        eventschoolstudent = EventSchoolStudent()
                        eventschoolstudent.student = student
                        eventschoolstudent.event_school = eventschool
                        eventschoolstudent.save()
                        send_mail = True
                    elif eventschoolstudent.active == False:
                        eventschoolstudent.active = True
                        eventschoolstudent.save()
                        send_mail = True
                    if send_mail:
                        signer = Signer()
                        code = base64.b64encode(signer.sign(str(student.pk)).encode('utf-8')).decode('utf-8')

                        event_title = event.name.encode('utf-8')
                        '''
                        if event.start_date.year == event.end_date.year:
                            event_title = event.start_date.strftime("%B") + ' - ' + event.end_date.strftime("%B") + ' ' + str(event.start_date.year)
                        else:
                            event_title = event.start_date.strftime("%B") + ' ' + str(event.start_date.year) + ' - ' + event.end_date.strftime("%B") + ' ' + str(event.end_date.year)
                        '''
                        template_info = {
                            'link': 'http://' + request.get_host() + '/download_qr/?code=' + code,
                            'first_name': student.first_name,
                            'email': student.email,
                            'event': event_title
                        }
                        sendToStudent.delay(template_info)
                EventSchoolStudent.objects.exclude(student__id__in=id_students).filter(event_school__event=event, event_school__school=school).update(active=False)
            else:
                EventSchoolStudent.objects.filter(event_school__event=event, event_school__school=school).update(active=False)

        EventSchool.objects.exclude(school__id__in=schools).filter(event=event).update(active=False)

        messages.success(request, 'Periodo actualizado satisfactoriamente')
        return redirect('/events/')

@method_decorator(csrf_exempt, name='dispatch')
class DownloadQRView(TemplateView):
    def get(self, request, *args, **kwargs):
        code = request.GET.get('code', '')
        url ="http://chart.apis.google.com/chart?cht=qr&chs=400x400&chl=" + code + "&chld=H|0";

        signer = Signer()
        id = 0
        try:
            id = signer.unsign(base64.b64decode(code))
        except:
            return render(request, 'QR_Comedor.html', { 'url': '', 'student': None })

        student = Student.objects.filter(pk=id).first()

        return render(request, 'QR_Comedor.html', { 'url': url, 'student': student })
    def post(self, request, *args, **kwargs):
        code = request.GET.get('code', '')

        signer = Signer()
        student = None
        try:
            id = signer.unsign(base64.b64decode(code))
            student = Student.objects.filter(pk=id).first()
        except:
            return render(request, 'QR_Comedor.html', { 'url': '', 'student': None })

        if student is None:
            return render(request, 'QR_Comedor.html', { 'url': '', 'student': None })

        path = os.path.join(settings.BASE_DIR, 'media', 'download_qr') + '/'
        filename = 'QR_Comedor_' + student.last_name + '_' + student.first_name + '.pdf'

        call(['xvfb-run', 'wkhtmltopdf', '--zoom', '2.8', request.build_absolute_uri(), path + filename])

        pdfFile = open((path + filename), 'rb')
        response = HttpResponse(pdfFile.read(), content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=%s' % smart_str(filename)

        return response

@method_decorator(csrf_exempt, name='dispatch')
class ValidateQR(View):
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body.decode("utf-8"))
        signer = Signer()
        try:
            sign = signer.unsign(request.META['HTTP_AUTHORIZATION'])
        except:
            return JsonResponse({ 'error': 'Usuario logueado incorrecto, favor de volver a iniciar sesión' })
        id, email = sign.split('-')
        user = User.objects.filter(pk=id, email=email).first()
        if user is None:
            return JsonResponse({ 'error': 'Usuario logueado incorrecto, favor de volver a iniciar sesión' })

        code = data['code']
        try:
            code = base64.b64decode(code).decode('utf-8')
            code = signer.unsign(code)
        except:
            return JsonResponse({ 'status': False, 'text': 'Codigo QR invalido' })

        student = Student.objects.filter(pk=code).first()
        if student is None:
            return JsonResponse({ 'status': False, 'text': 'Codigo del estudiante invalido' })

        eventstudent = EventSchoolStudent.objects.filter(student=student, event_school__event__start_date__lte=timezone.now(), event_school__event__end_date__gte=timezone.now(), active=True).order_by('-event_school__event__start_date').first()
        if eventstudent is None:
            return JsonResponse({ 'status': False, 'text': 'El estudiante no se encuentra dentro de algun periodo activo' })

        if StudentEvent.objects.filter(event_student=eventstudent, date=timezone.now()).exists():
            return JsonResponse({ 'status': False, 'text': 'Este estudiante ya hizo su solicitud el dia de hoy' })

        studentevent = StudentEvent()
        studentevent.event_student = eventstudent
        studentevent.save()
        return JsonResponse({ 'status': True, 'text': 'Codigo valido para ' + student.first_name + ' ' + student.last_name })

@method_decorator(csrf_exempt, name='dispatch')
class ReportView(TemplateView):
    template_name = 'event_report.html'
    def get(self, request, pk, *args, **kwargs):
        event = get_object_or_404(Event, pk=pk)
        return render(request, self.template_name, { 'event': event })
    def post(self, request, pk, *args, **kwargs):
        path = os.path.join(settings.BASE_DIR, 'media', 'pdf') + '/'
        filename = str(pk) + '.pdf'

        print(request.build_absolute_uri())
        call(['xvfb-run', 'wkhtmltopdf', '--zoom', '2.8', request.build_absolute_uri(), path + filename])

        pdfFile = open((path + filename), 'rb')
        response = HttpResponse(pdfFile.read(), content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=%s' % smart_str(filename)

        return response