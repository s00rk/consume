from django.contrib import admin

# Register your models here.
from .models import UserInfo, Event, EventSchool, EventSchoolStudent, StudentEvent

admin.site.site_header = 'Gestor de comedores'

@admin.register(UserInfo)
class EventAdmin(admin.ModelAdmin):
    list_display = ('id', 'user')
    search_fields = ['user']

@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'start_date', 'end_date')
    search_fields = ['name']

@admin.register(EventSchool)
class EventSchoolAdmin(admin.ModelAdmin):
    list_display = ('id', 'event', 'school', 'active')
    raw_id_fields = ('event', 'school')
    search_fields = ['event', 'school']

@admin.register(EventSchoolStudent)
class EventSchoolStudentAdmin(admin.ModelAdmin):
    list_display = ('id', 'event_school', 'student', 'active')
    raw_id_fields = ('event_school', 'student')
    search_fields = ['event_school', 'student']

@admin.register(StudentEvent)
class StudentEventAdmin(admin.ModelAdmin):
    list_display = ('id', 'event_student', 'date')
    raw_id_fields = ('event_student',)
