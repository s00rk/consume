# coding=utf-8
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings

from consume.celery import app

@app.task(name='send_email_to_user_reset')
def sendToUserReset(template_info):
    subject, from_email, to = 'Restaurar contraseña Gestor de Comedores', settings.EMAIL_HOST_USER, template_info['email']

    html_content = render_to_string('email_user_reset.html', template_info)
    text_content = strip_tags(html_content)

    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

@app.task(name='send_email_to_user')
def sendToUser(template_info):
    subject, from_email, to = 'Bienvenido al Gestor de Comedores', settings.EMAIL_HOST_USER, template_info['email']

    html_content = render_to_string('email_user.html', template_info)
    text_content = strip_tags(html_content)

    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

@app.task(name='send_email_to_student')
def sendToStudent(template_info):
    subject, from_email, to = 'Bienvenido al Gestor de Comedores', settings.EMAIL_HOST_USER, template_info['email']

    html_content = render_to_string('email_student.html', template_info)
    text_content = strip_tags(html_content)

    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

@app.task(name='send_email_to_school')
def sendToSchool(template_info):
    subject, from_email, to = 'Bienvenido al Gestor de Comedores', settings.EMAIL_HOST_USER, template_info['email']

    html_content = render_to_string('email_school.html', template_info)
    text_content = strip_tags(html_content)

    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()
